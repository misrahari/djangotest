from django.forms import widgets
from rest_framework import serializers
from models import UserM


class UserSerializer(serializers.Serializer):
    name = serializers.CharField(required=True, allow_blank=False, max_length=100)
    created_at = serializers.CharField(required = True)

