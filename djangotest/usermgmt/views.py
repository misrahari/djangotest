from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from models import UserM
from django.http import HttpResponse
from django.views.generic import View
from serializer import * 
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class UserNewView(View):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
	
	data = {}	

        return render(request, self.template_name, {'formdata':data })

    def post(self, request, *args, **kwargs):

	
	name =  request.POST['username']
	usr = UserM()
	usr.name = name
	usr.save()
        return render(request, self.template_name, {'form': name})


@csrf_exempt
def user_list(request):
    """
    List all users.
    """
    if request.method == 'GET':
        users = UserM.objects.all()
        serializer = UserSerializer(users, many=True)
        return JSONResponse(serializer.data)


# Create your views here.
