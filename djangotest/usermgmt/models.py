from django.db import models
from django.contrib import admin




class UserM(models.Model):

    name = models.CharField(max_length = 50)
    created_at = models.DateTimeField(auto_now_add=True)
	
    def __unicode__(self):
	return self.name



admin.site.register(UserM)

# Create your models here.
