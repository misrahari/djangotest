from django.conf.urls import patterns, include, url
from django.contrib import admin
from usermgmt.views import UserNewView,user_list

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djangotest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^users/$', user_list),
    url(r'^au/', UserNewView.as_view()),
    url(r'^admin/', include(admin.site.urls)),
)
